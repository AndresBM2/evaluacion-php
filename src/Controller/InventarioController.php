<?php

namespace App\Controller;

use App\Entity\Inventario;
use Doctrine\Persistence\ManagerRegistry;
use PhpParser\Node\Expr\Cast\Int_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InventarioController extends AbstractController
{
    /**
     * @Route("/inventario", name="app_inventario")
     */
    public function index(ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        $getInventario = $em->getRepository(Inventario::class)->findAll();

        return $this->render('inventario/index.html.twig', [
            'controller_name' => 'Lista de Inventario',
            'Inventario' => $getInventario,
        ]);
    }

    /**
     * @Route("/registrar", name="app_registar")
     */

    public function registrar(ManagerRegistry $doctrine): Response
    {
        $titulo = $_POST["titulo"];
        $descripcion = $_POST["descripcion"];
        $anioPublicacion = $_POST["publicacion"];
        $autor = $_POST["autor"];

        $inv = new Inventario();

        $inv->setTitulo($titulo);
        $inv->setDescripcion($descripcion);
        $inv->setAnioPublicacion($anioPublicacion);
        $inv->setAutor($autor);

        $em = $doctrine->getManager();

        $em->persist($inv);
        $em->flush();

        return $this->render('inventario/registro.html.twig', [
            'mensaje' =>'Registro Insertado con exito',
        ]);
    }

    /**
     * @Route("/eliminar/{idInventario}", name="app_eliminar")
     */

    public function eliminar(ManagerRegistry $doctrine, $idInventario): Response
    {

        $em = $doctrine->getManager();

        $getInventario = $em->getRepository(Inventario::class)->findOneBy(
            [ 'id' => $idInventario,]
        );

        $em->remove($getInventario);
        $em->flush();

        return $this->render('inventario/registro.html.twig', [
            'mensaje' =>'Inventario Eliminado',
        ]);
    }

    /**
     * @Route("/actualizar/{idInventario}", name="app_actualizar")
     */

    public function actualizar(ManagerRegistry $doctrine, $idInventario): Response
    {
        $titulo = $_POST["titulo"];
        $descripcion = $_POST["descripcion"];
        $anioPublicacion = $_POST["publicacion"];
        $autor = $_POST["autor"];

        $em = $doctrine->getManager();

        $getInventario = $em->getRepository(Inventario::class)->findOneBy(
            [ 'id' => $idInventario,]
        );

        $getInventario->setTitulo($titulo);
        $getInventario->setDescripcion($descripcion);
        $getInventario->setAnioPublicacion($anioPublicacion);
        $getInventario->setAutor($autor);

        $em->persist($getInventario);
        $em->flush();

        return $this->render('inventario/registro.html.twig', [
            'mensaje' =>'Registro Actualizado con exito',
        ]);
    }
}
