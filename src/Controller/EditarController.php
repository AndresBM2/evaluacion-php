<?php

namespace App\Controller;

use App\Entity\Inventario;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditarController extends AbstractController
{
    /**
     * @Route("/editar/{idInventario}", name="app_editar")
     */
    public function index(ManagerRegistry $doctrine, $idInventario): Response
    {

        $em = $doctrine->getManager();

        $getInventario = $em->getRepository(Inventario::class)->findOneBy(
            [ 'id' => $idInventario,]
        );

        return $this->render('editar/index.html.twig', [
            'controller_name' => 'EditarController',
            'Inventario' => $getInventario,
        ]);
    }
}
